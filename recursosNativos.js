//En este archivo manejamos los recursos del dispositivo, como ser la geolocalizacion o cámara

let geoLoc = document.querySelector('#geoLoc'); // Seleccionamos el elemento 'geoLoc' donde mostraremos el mapa de Google
function mostrarGeolocalizacion() {
    //La funcion se encarga de mostrar el mapa (si el dispositivo lo soporta)
    if (navigator.geolocation) {
        const googleMapKey = 'AIzaSyA5mjCwx1TRLuBAjwQw84WE6h5ErSe7Uj8';
        let message = "";
        navigator.geolocation.getCurrentPosition(pos => {
            message = ` <iframe
                            width="100%"
                            height="250"
                            frameborder="0" style="border:0"
                            src="https://www.google.com/maps/embed/v1/view?key=${ googleMapKey}&center=${pos.coords.latitude},${pos.coords.longitude}&zoom=17" allowfullscreen>
                        </iframe>`;
            geoLoc.innerHTML = message;
        });
    }
    else
        console.log("No sse puede usar la geolocalizacion :(")
}
function ocultarGeolocalizacion() {
    //Ocultamos el mapa de google
    geoLoc.innerHTML = "";
}
// En esta seccion configuramos las opciones para mostrar y ocultar la camara
let miCamara = document.querySelector('#miCamara'); // Seleccionamos el elemento 'geoLoc' donde mostraremos la camara
let miStream;
function mostrarCamara() {
    console.log("Intentando acceder a la camara...");

    if (navigator.mediaDevices) {
        navigator.mediaDevices.getUserMedia({
            audio: false,
            video: { width: 300, height: 300 }
        }).then(stream => {
            miStream = stream;
            miCamara.srcObject = miStream;
        }).catch(err=>{
            console.log('mediaDevices: Ocurrio un error;', err)
        })
    }
    else
        console.log("No se puede usar la camara, amiguito :(");

}
function ocultarCamara() {
    console.log('Ocultando la camara.. :0');
    miCamara.pause();
    if (miStream)
        miStream.getTracks()[0].stop();
    else
        console.log("Stream no existe");
        
    
}